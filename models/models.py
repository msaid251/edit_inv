# -*- coding: utf-8 -*-

from odoo import models, fields, api

class stock_pack_operation(models.Model):
    _inherit = 'stock.pack.operation'
    qty_available = fields.Float(related='product_id.qty_available')

class stock_move(models.Model):
    _inherit = 'stock.move'
    qty_available = fields.Float(related='product_id.qty_available')
